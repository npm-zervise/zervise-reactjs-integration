# Zervise Chat Integration for React

A NPM JS React package for easily integrating any app with Zervise's customer support and ticketing system with in built chat support for bringing the easy setup and best experience.

## Table of Contents
- [Zervise](#zervise)
- [Features](#features)
- [Installing](#installing)
- [Your Zervise Subdomain](#your-zervise-subdomain)
- [Examples](#examples)
- [License](#license)

## Zervise

- Add an efficient ticketing system for free, Free Plan comes with No credit card, upto 5 agents 
- Unlimited Users
- Unlimited Tickets
- SLA Management
- Email Notification
- Ticket Creation by Web
- Ticket creation through Chat
- Real Time Chat with Agent
- Real Time Ticket Update

## Features

- Integrate your [Zervise](https://zervise.com) subdomain with your other app.
- Authenticate your users in Zervise subdomain with their email.
- Add an efficient ticketing system for your customers.
- Create ticket with attachments.
- View tickets to manage your customer complaints.
- Interact directly using embedded Chat system with attachments.
- Show the FAQs for your Zervise subdomain in your other apps.

## Installing

- Using npm:

```bash
$ npm install @zervise/zervise-chat
```

- Using yarn:

```bash
$ yarn add @zervise/zervise-chat
```

## Your Zervise Subdomain

### Sign Up in Zervise

For using this package you need a Zervise account and a Zervise subdomain.<br>
To create a `Free Zervise Account and get your own Zervise subdomain` head over to this link [Sign Up in Zervise](https://zervise.com/).

### Find your Zervise subdomain

Upon succesfull sign up you should receive one email with your zervise subdomain link in the registered email address.

#### Example

If your link is `https://zervisefree-y9gwml4zx4ap.zervise.com/`, then your zervise subdoamin is `zervisefree-y9gwml4zx4ap`.

## Examples

### Import to your project

```js
import { Zervise } from "@zervise/zervise-chat";
```

### Authenticate

- After importing use directly to your project

```js
  <Zervise 
    subdomain="<Your-Subdomain>" 
    name="<user-name>" 
    email="<user-email>" 
    position= "left"|"center"|"right" 
  />
```

- Upon successful authentication, you would get this UI

  ![image](https://drive.google.com/uc?export=view&id=1D1N0SNZ9wzA2q1nmsJnvCpzC2YcUQmP5)


### Create a new ticket

- You can create new ticket using the following and jump to the [My_Tickets](#mytickets)

  ![image](https://drive.google.com/uc?export=view&id=1r6mB-oYzY1cWfSB27AIuXO9JFm-dYWTw)

### Your Tickets

- All your tickets can be found here

  ![image](https://drive.google.com/uc?export=view&id=14IVzojyqdRGATbD_cVAVWtYRk9gBkYCK)

### Reply to a ticket as a user

- Get all the activities of individual tickets.

  ![image](https://drive.google.com/uc?export=view&id=1WqiFoRZWaeRrnrgoEErk9Wuf2lYeZn--)

### FAQ articles of your company for users

- Get all the FAQs and their details

  ![image](https://drive.google.com/uc?export=view&id=1KEfFoc4LK3kWkfoUHvZbVksVJ2_nts0J)

  ![image](https://drive.google.com/uc?export=view&id=1BCIG00UpZbFJh_YrJhtXFrV1IKrLTtZ2)


## Resources

- [Video Guides](https://zervise.com/resources/videos)
- [Beginners guide for Zervise admin](https://zervise.com/resources/beginners_guide/zervisesignup)
  - [Zervise Signup](https://zervise.com/resources/beginners_guide/zervisesignup)
  - [Tickets](https://zervise.com/resources/beginners_guide/tickets)
  - [Agents](https://zervise.com/resources/beginners_guide/agents)
  - [Users](https://zervise.com/resources/beginners_guide/users)
  - [User Signup and Ticket Creation](https://zervise.com/resources/beginners_guide/user_signup_and_ticket_creation)
  - [Agent Signup Process](https://zervise.com/resources/beginners_guide/agent_signup)
  - [Ticket Status](https://zervise.com/resources/beginners_guide/ticket_status)
  - [Teams](https://zervise.com/resources/beginners_guide/teams)
  - [Services](https://zervise.com/resources/beginners_guide/services)
  - [Priority List](https://zervise.com/resources/beginners_guide/priority)

## Issues

If you encounter any issue while using the package please report it here 👉 [Zervise API > Issues](https://gitlab.com/npm-zervise/zervise-reactjs-integration/-/issues)

## License

[MIT License](https://gitlab.com/npm-zervise/zervise-reactjs-integration/-/blob/master/LICENSE)