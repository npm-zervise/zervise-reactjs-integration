import React, { useEffect, useState } from 'react'
import { io } from 'socket.io-client'
import axios from 'axios'
import './style.css';

import Support from './Support'
import Requirements from './Requirements'

const Zervise = ({ subdomain, name, email, position }) => {

    const [resultantData, setResultantData] = useState({})
    const [authenticated, setAuthenticated] = useState(false)
    const [error, setError] = useState(false)
    const [visible, setVisible] = useState(true)
    // eslint-disable-next-line
    const [fetchedAgent, setFetchedAgent] = useState(false)
    const [agent, setAgent] = useState({})
    const [clicked, setClicked] = useState(false);

    const appName = 'external app'
    const mobile = ''
    const apiBase = 'https://api.zervise.com'

    const socket = io(apiBase)  

    console.log('Agent: ', agent)


    const authenticate = async () =>
    {
      try {
        const { data } = await axios({
          method: 'post',
          url: `https://api.zervise.com/auth/user/external-auth/${subdomain}`,
          data: {
            name,
            email,
            appName,
            mobile,
          }
        })
        if(data) setResultantData(data)
        if(data)setAuthenticated(true)
        if(authenticated) getAgents()
        
      } catch (error) {
        // console.log(error.response.status);
        setError(true)
      }
    }
      
      
    const getAgents = async () =>
    {
      const { data } = await axios({
        method : 'GET',
        url : `${apiBase}/person/clientId/${resultantData.person.clientId}`,
        headers : {
          'auth-token': resultantData.token,
          clientId: resultantData.person.clientId,
        }
      })   
      
      setAgent(data)
      setFetchedAgent(true)
    }
      
    const closeButton = () => {
      setVisible(false)
    }
    
    useEffect(() => {
        authenticate();
        // eslint-disable-next-line
    }, [authenticated])

    return (
      <>
        <div 
          className={ 
            visible ? `err_msg active ${position}` : "err_msg disable"
          }
        >
          {
            error
            ? <div className="alert alert-danger">
                <span 
                  class="closebtn" 
                  onClick={closeButton}>
                    &times;
                </span>  
                <strong>User Not Authenticated!
                <br />
                </strong> Check with your Admin.
              </div>
            : ''
          }
        </div>

        <div 
          style={{
            visibility : authenticated ? 'visible' : 'hidden'
          }}
        >
          {
            clicked 
            ? 
              <Requirements 
                socket={socket} 
                subdomain={subdomain} 
                agent={agent} 
                apiBase={apiBase} 
                result={resultantData} 
                token={resultantData.token} 
                setClick={setClicked}
                position={position}
              /> 
            : 
              <Support 
                position={position} 
                setClick={setClicked}
              />
          }
        </div>
        
    </>
    )
}

export default Zervise
